<x-layout title="Job Details">
    <x-slot name="heading">
        Job Details
    </x-slot>
    <h1>Job Title: <span class="font-bold">{{ $job['title'] }}</span></h1>
    <p>Salary: <span class="font-bold">$ {{ number_format($job['salary'], 2) }}</span></p>
</x-layout>