<x-layout title="Home">
    <x-slot name="heading">
        Home
    </x-slot>
    
    <ul>
        @foreach ($jobs as $job)
            <li>
                <a href="{{ route('job.show', ['id' => $job['id']]) }}" class="text-blue-400 hover:underline">
                    {{ $job['title'] }} - ${{ number_format($job['salary'], 2) }}
                </a>
            </li>
        @endforeach
    </ul>
</x-layout>